# 作业要求

## Definition of Done

* 必须 **非常严格** 的按照题目中的定义进行实现。
* 程序必须能够在真实 HTTP Client 下工作。
* 必须有单元或者集成测试。每一个测试要代表一个 Task（这意味着你的task需要足够小），每一个测试通过之后需要进行一次 commit（小步提交）。
* 测试至少要覆盖“业务要求”提到的问题。
* 注意代码的坏味道。尤其是命名的规范性以及重复代码的问题。
* 该练习 **无需考虑** 并发问题。

# 业务要求

我们 “**愛颩尅谂**” 集团的产品做的实在是太成功了。因此想加盟的商家络绎不绝。但是如果要加盟的话需要我们来审核资质。由于大家意向爆棚，不得不提前预约（reservation）来审核资质。于是，我们计划再做一款预约 App 来满足用户的要求。

为了说明的方便，我们定义了如下的 Persona（Persona 是用户画像/用户模型，根据不用用户的特点将所有用户分门别类后，为每一个分类起一个名字，一般使用常见的人名。虽然使用的是人名，但一个Persona指代的仍是一类用户，而不是特定的某一个用户或人）：

> 重要的事情说三遍：
> * Persona 是 **一类用户！**，**不是** 一个人。
> * Persona 是 **一类用户！**，**不是** 一个人。
> * Persona 是 **一类用户！**，**不是** 一个人。

* Rob：这是教育集团方的资质审核人员(Staff)，希望加盟的客户会预约他们的时间。

<img alt="staff" src="./asset/staff.png"/>

* Sofia：这是希望加盟的客户方的代表。他会和 Rob 预约资质审核的时间。

<img alt="sofia" src="./asset/sofia.png">

## Story 1 添加或获取 Rob 用户信息

我们应当可以在系统中添加 Rob 用户。这样用户就可以向指定的 Rob 提交资质审核预约了。

### AC 1：向系统中添加 Rob 用户

系统应当提供如下的 API 向其中添加 Rob 用户。系统中不止有一个 Rob 用户。

|项目|说明|
|---|---|
|URI|`/api/staffs`|
|HTTP Method|`POST`|
|`Content-Type`|`application/json`|

其 Content 定义如下：

```json
{
  "firstName": "Rob",   /* First name */
  "lastName": "Hall",   /* Last name */
}
```

其中

|项目|说明|
|---|---|
|`firstName`|必须提供。且最大不能够超过 64 个字符|
|`lastName`|必须提供。且最大不能超过 64 个字符|

如果请求无法满足上述信息，则其响应为：

|项目|说明|
|---|---|
|Status Code|`400`|

如果用户创建成功。则其响应为：

|项目|说明|
|---|---|
|Status Code|`201`|
|`Location`|`/api/staffs/{staffId}`|

其中 `staffId` 是每一个 Rob 用户的唯一标识。必须是数字。例如：`/api/staffs/2`

### AC 2：获取 Rob 用户的信息

我们可以使用如下的 API 来获得特定 Rob 用户的信息：

|项目|说明|
|---|---|
|URI|`/api/staffs/{staffId}`|
|HTTP Method|`GET`|

其响应如下：

|项目|说明|
|---|---|
|Status Code|`200`|
|`Content-Type`|`application/json`|

其 Content 为：

```json
{
  "id": 2,              /* Id of Rob */
  "firstName": "Rob",   /* First name */
  "lastName": "Hall",   /* Last name */
}
```

如果 `{staffId}` 不存在，则该 API 响应的 Status Code 为 `404`。

### AC 3：获得所有的 Rob 用户信息

我们可以使用如下的 API 来获得系统中所有的 Rob 用户的信息：

|项目|说明|
|---|---|
|URI|`/api/staffs`|
|HTTP Method|`GET`|
|`Content-Type`|`application/json`|

该 API 将返回所有的 Rob 用户的信息。并按照 `id` 进行排序。例如：

```json
[
  {
  "id": 2,              /* Id of Rob */
  "firstName": "Rob",   /* First name */
  "lastName": "Hall",   /* Last name */
  },
  ...
]
```

如果系统中并不存在任何的 Rob 用户，则返回空的 JSON 数组：

```json
[]
```

## Story 2 设置 Rob 的时区信息

由于我们家大业大。所以 Rob 的工作地点可能在全球的任何地方。为了方便大家预约，Rob 需要登记自己目前所在的时区。这样客户预约的时候就能够知道 Rob 所在地的时间，从而选择合适的时间段。

注：你可以通过如下方式获得 Java 中支持的所有时区 Id 的列表：

```java
ZoneRulesProvider.getAvailableZoneIds()
```

### AC 1：为 Rob 用户设置时区信息

我们需要更新 Rob 的时区信息。因此我们添加了如下的 API

|项目|说明|
|---|---|
|URI|`/api/staffs/{staffId}/timezone`|
|HTTP Method|`PUT`|
|`Content-Type`|`application/json`|

其 Content 的内容为：

```json
{
  "zoneId": "Asia/Chongqing"  /* timezone */
}
```

如果成功更新，则响应为：

|项目|说明|
|---|---|
|Status Code|`200`|

如果不提供 `zoneId` 或者 `zoneId` 名称不正确（大小写敏感）。则响应为：

|项目|说明|
|---|---|
|Status Code|`400`|

### AC 2：在获得 Rob 用户信息时返回时区信息

而相应的在获得 Rob 用户的信息时，返回的 Content 也应当包含这个信息：

```json
{
  "id": 2,                    /* Id of Rob */
  "firstName": "Rob",         /* First name */
  "lastName": "Hall",         /* Last name */
  "zoneId": "Asia/Chongqing"  /* Timezone */
}
```

需要注意的是，如果该用户没有指定时区信息，则相应的 `zoneId` 应当返回 `null`。例如：

```json
{
  "id": 2,                    /* Id of Rob */
  "firstName": "Rob",         /* First name */
  "lastName": "Hall",         /* Last name */
  "zoneId": null              /* No timezone speicified */
}
```

## Story 3 获得时区列表

作为 Rob 和 Sofia 我希望能够获得一个时区列表，以便我能够恰当的选择时区。

### AC 1：获得时区列表

Rob 和 Sofia 都应当可以通过如下 API 获得一个系统支持的时区列表。

|项目|说明|
|---|---|
|URI|`/api/timezones`|
|HTTP Method|`GET`|

该 API 将返回一个时区列表。里面包含所有的 `zoneId`。其响应的格式为：

|项目|说明|
|---|---|
|Status Code|`200`|
|`Content-Type`|`application/json`|

其 Content 为一个 JSON 数组，里面包含所有的 `zoneId`，并按照字母顺序升序排序。例如：

```json
[
  "Africa/Abidjan",
  "Africa/Accra",
  "Africa/Addis_Ababa",
  "Africa/Algiers",
  "Africa/Asmara",
  "Africa/Asmera",
  "Africa/Bamako",
  "Africa/Bangui",
  ...
]
```

## Story 4 Sofia 预约 Rob 用户进行审核

作为 Sofia 用户，我应当能够预约 Rob 用户的时间以便进行在线审核。

### AC 1：Sofia 添加预约

Sofia 用户可以通过如下的 API 添加一个预约：

|项目|说明|
|---|---|
|URI|`/api/staffs/{staffId}/reservations`|
|HTTP Method|`POST`|
|`Content-Type`|`application/json`|

例如 `/api/staffs/2/reservations`

其 Content 为：

```json
{
  "username": "Sofia",
  "companyName": "ThoughtWorks",
  "zoneId": "Africa/Nairobi",
  "startTime": "2019-08-20T11:46:00+03:00",
  "duration": "PT1H"
}
```

<img alt="sofia" src="./asset/sofia.png">

以上请求的意思是：来自 ThoughtWorks 的 Sofia，希望预约 2 号 Rob 的时间。我所在的时区是 *Africa/Nairobi* 。我希望审核从 *2019-08-20T11:46:00+03:00* 开始，持续时间为 1 小时。

其中：

|项目|说明|约束|
|---|---|---|
|`username`|Sofia 用户的姓名|必须提供，最大长度 128|
|`companyName`|Sofia 的公司名称|必须提供，最大长度 64|
|`zoneId`|Sofia 所在的时区。注意和 Rob 的时区不一定一样|必须提供，必须为支持的 IANA 时区的名称|
|`startTime`|审核开始时间。为 ISO 8601 标准的日期时间。|必须提供，格式为 `YYYY-MM-DDThh:mm:ss±hh`，请参见 ISO 8601 文档 Section 4.3.2. 注意，这个时间不一定是 `zoneId` 指定的时区的当地时间。例如，如果 `zoneId` 为 `Africa/Nairobi`（当地的 offset 为 `+03:00`），我也可以指定 `startTime` 为 `2019-08-20T16:46:00+08:00`。`zoneId` 是为了方便时间的显示。|
|`duration`|审核持续时间，为 ISO 8601 标准的 Duration 时间。|必须提供，格式为 `PT?H`。请参见 ISO 8601 文档，Section 4.4.3.2。预约时长只能是整小时数，而且只能是 1-3 小时|

如果创建成功则应当返回如下的响应：

|项目|说明|
|---|---|
|Status Code|`201`|
|`Location`|`/api/staffs/{staffId}/reservations`|

如果请求中的内容不符合上述表格中的内容，则响应为：

|项目|说明|
|---|---|
|Status Code|`400`|

> 注：必须使用正确的 Java 8 日期时间类型来表示相应的数据。例如 `Instant`, `OffsetDateTime`, `ZonedDateTime`, `Duration` 等。详情可以查询相应文档。

### AC 2：获得预约列表

相应的，Sofia 和 Rob 可以通过

|项目|说明|
|---|---|
|URI|`/api/staffs/{staffId}/reservations`|
|HTTP Method|`GET`|

获得指定的 Rob 身上的所有预约列表（例如 `/api/staffs/2/reservations`）。其响应如下：

|项目|说明|
|---|---|
|Status Code|`200`|
|`Content-Type`|`application/json`|

其 Content 为 JSON 数组，例如：

```json
[
  {
    "username": "Sofia",
    "companyName": "ThoughtWorks",
    "duration": "PT1H",
    "startTime": {
      "client": {
        "zoneId": "Africa/Nairobi",
        "startTime": "2019-08-20T11:46:00+03:00"
      },
      "staff": {
        "zoneId": "Asia/Chongqing",
        "startTime": "2019-08-20T16:46:00+08:00"
      }
    }
  }
]
```

这个响应中的预约的含义是来自 *ThoughtWorks* 的 *Sofia* 预约了 2 号 *Rob* 的时间进行审核，审核持续时间为 1 小时。目前，Rob 的时区为 *Asia/Chongqing*。因此，审核开始时间为 *Africa/Nairobi* 时间 2019-08-20 11:46；*Asia/Chongqing* 时间 2019-08-20 16:46。如果有多个预约则按照 `startTime` 进行排序。

以下为详细说明

|项目|说明|
|---|---|
|`username`|Sofia 用户的姓名|
|`companyName`|Sofia 用户的公司名称|
|`duration`|审核持续的时间。格式为 `PT?H`。请参见 ISO 8601 文档，Section 4.4.3.2|
|`startTime.client`|以 Sofia 的时区为准的审核开始时间。其中 `zoneId` 为 Sofia 的时区。而 `startTime` 为 Sofia 所在时区的 **当地时间**。该时间为 ISO 8601 标准的日期时间。格式为 `YYYY-MM-DDThh:mm:ss±hh`，请参见 ISO 8601 文档 Section 4.3.2|
|`startTime.staff`|以 Rob 的时区为准的审核开始时间。其中 `zoneId` 为 Rob 的 **当前** 时区。`startTime` 为 Rob 当前时区的 **当地时间**。该时间为 ISO 8601 标准的日期时间。格式为 `YYYY-MM-DDThh:mm:ss±hh`，请参见 ISO 8601 文档 Section 4.3.2|

### AC 3：失败的预约

预约必须满足如下的规则，否则会失败。例如：

<img alt="staff" src="./asset/staff.png"/>

**规则1：Rob 用户必须设置了时区**

如果指定的 Rob 用户没有设置时区，则说明 Rob 还没有准备好为 Sofia 服务。所有的申请会失败。响应为：

|项目|说明|
|---|---|
|Status Code|`409`|
|`Content-Type`|`application/json`|
|Content|`{"message": "{firstName lastName} is not qualified."}`|

**规则2：提前两天预约**

如果指定的预约时间距离当前时间 48 小时之内（含 48 小时）。则申请失败。响应为：

|项目|说明|
|---|---|
|Status Code|`400`|
|`Content-Type`|`application/json`|
|Content|`{"message": "Invalid time: the application is too close from now. The interval should be greater than 48 hours."}`|

**规则3：预约时间和持续时间不能够和 Rob 已有的预约冲突**

如果指定的预约时间和持续时间与 Rob 已有的预约冲突，则申请失败。响应为：

|项目|说明|
|---|---|
|Status Code|`409`|
|`Content-Type`|`application/json`|
|Content|`{"message": "The application is conflict with existing application."}`|

**规则4：预约的开始时间必须在 Rob 的工作时间之内**

预约的开始时间必须在 Rob 的工作时间之内。Rob 每天的工作时间在 **当地时间** 早晨 09:00-17:00。否则申请失败。响应为：

|项目|说明|
|---|---|
|Status Code|`409`|
|`Content-Type`|`application/json`|
|Content|`{"message": "You know, our staff has their own life."}`|

**规则5：预约时长只能是整小时数，而且只能是 1-3 小时**

我们特别害怕把 Rob 累着然后他们会离职。因此如果在 1-3 小时外则申请失败。响应为：

|项目|说明|
|---|---|
|Status Code|`400`|
|`Content-Type`|`application/json`|
|Content|`{"message": "The application duration should be within 1-3 hours."}`|
