package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.contract.ZoneId;
import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class ZoneControllerTest extends ApiTestBase {
    @Autowired
    private ObjectMapper mapper = new ObjectMapper();
    @Autowired
    StaffRepository staffRepository;

    @Test
    void should_return_status_200_when_update_zoneId_() throws Exception {
        ZoneId zoneId = new ZoneId("Asia/Chongqing");

        mockMvc.perform(put("/api/staffs/1/timezone")
                .content(mapper.writeValueAsString(zoneId))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_status_400_when_zoneId_is_not_correct() throws Exception {
        ZoneId zoneId = new ZoneId();

        mockMvc.perform(put("/api/staffs/1/timezone")
                .content(mapper.writeValueAsString(zoneId))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_Rob_info_with_zoneId_when_get_Rob() throws Exception {
        staffRepository.saveAndFlush(new Staff("Rob","Hall","Asia/Chongqing"));
        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.firstName").value("Rob"))
                .andExpect(jsonPath("$.lastName").value("Hall"))
                .andExpect(jsonPath("$.zoneId").value("Asia/Chongqing"));
    }
}
