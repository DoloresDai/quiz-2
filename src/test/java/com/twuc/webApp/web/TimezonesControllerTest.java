package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.domain.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.zone.ZoneRulesProvider;
import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;


class TimezonesControllerTest extends ApiTestBase {
    //TODO: unused field
    @Autowired
    private ObjectMapper mapper = new ObjectMapper();
    @Autowired
    StaffRepository staffRepository;

    @Test
    void should_return_all_timezones() throws Exception {
        Set<String> zoneIds = ZoneRulesProvider.getAvailableZoneIds();
        mockMvc.perform(get("/api/timezones"))
                .andExpect(content().string(serialize(zoneIds)));
    }
}
