package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.contract.Name;
import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class StaffControllerTest extends ApiTestBase {
    @Autowired
    private ObjectMapper mapper = new ObjectMapper();
    @Autowired
    StaffRepository staffRepository;

    @Test
    void should_return_status_201_and_location_when_add_new_Rob_successfully() throws Exception {

        Name name = new Name("Rob", "Hall");

        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(name)))
                .andExpect(status().is(201))
                .andExpect(header().string("Location", "http://localhost/api/staffs/1"));
    }

    @Test
    void should_return_status_400_when_post_new_Rob_unsuccessfully() throws Exception {
        Name name = new Name(null, null);

        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(name)))
                .andExpect(status().is(400));
    }

    @Test
    void should_get_Rob_Info_when_get() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        staffRepository.saveAndFlush(staff);

        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.firstName").value("Rob"))
                .andExpect(jsonPath("$.lastName").value("Hall"));
    }

    @Test
    void should_return_status_404_when_cannot_get_Rob_info() throws Exception {
        mockMvc.perform(get("/api/staffs/2"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_get_all_robs_info() throws Exception {
        mockMvc.perform(get("/api/staffs"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("[{\"firstName\":\"Rob1\",\"lastName\":\"Hall\",\"id\":1},{\"firstName\":\"Rob2\",\"lastName\":\"Hall\",\"id\":2}]"));
    }

}
