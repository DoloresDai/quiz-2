package com.twuc.webApp.contract;

//TODO: unused import
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
public class Name{
    @NotNull
    @Size(max = 64)
    private String firstName;
    @NotNull
    @Size(max = 64)
    private String lastName;

    public Name() {
    }

    public Name(@NotNull @Size(max = 64) String firstName, @NotNull @Size(max = 64) String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
