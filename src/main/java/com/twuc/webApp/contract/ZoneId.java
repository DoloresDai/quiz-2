package com.twuc.webApp.contract;

import javax.validation.constraints.NotNull;

public class ZoneId {
    @NotNull
    private String zoneId;

    public ZoneId() {
    }

    public ZoneId(@NotNull String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }
}
