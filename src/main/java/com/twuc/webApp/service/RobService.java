package com.twuc.webApp.service;

import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.StaffRepository;
import com.twuc.webApp.web.exceptions.RobNotFundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RobService {
    //TODO: field injection is not recommended
    @Autowired
    private StaffRepository staffRepository;

    public void saveAndFlushRob(Staff staff){
        staffRepository.saveAndFlush(staff);
    }

    public Staff findById(Long staffId) throws RobNotFundException {
       return staffRepository.findById(staffId).orElseThrow(RobNotFundException::new);
    }
}
