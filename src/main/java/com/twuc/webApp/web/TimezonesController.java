package com.twuc.webApp.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.zone.ZoneRulesProvider;
import java.util.Set;

@RestController
@RequestMapping("/api/")
public class TimezonesController {
    @GetMapping("timezones")
    public ResponseEntity<Set<String>> getAllTimezones() {
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(availableZoneIds);
    }
}
