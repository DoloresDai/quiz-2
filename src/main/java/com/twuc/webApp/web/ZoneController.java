package com.twuc.webApp.web;

import com.twuc.webApp.contract.ZoneId;
import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.service.RobService;
import com.twuc.webApp.web.exceptions.RobNotFundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/")
public class ZoneController {
    @Autowired
    RobService robService;

    @PutMapping("staffs/{staffId}/timezone")
    public ResponseEntity<Staff> updateZoneId(@PathVariable Long staffId, @RequestBody @Valid ZoneId zoneId) throws RobNotFundException {
        robService.saveAndFlushRob(new Staff("Rob", "Hall"));
        Staff rob = robService.findById(staffId);
        rob.setZoneId(zoneId.getZoneId());
        robService.saveAndFlushRob(rob);
        return ResponseEntity.status(HttpStatus.OK).body(rob);
    }

    //TODO: use exception advisor 因为两个controller都处理了RobNotFundException
    @ExceptionHandler(RobNotFundException.class)
    public ResponseEntity<String> handleZoneIdIsNotCorrect() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }


}
