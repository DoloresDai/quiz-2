package com.twuc.webApp.web;

import com.twuc.webApp.contract.Name;
import com.twuc.webApp.contract.ZoneId;
import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.service.RobService;
import com.twuc.webApp.web.exceptions.RobNotFundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
//TODO: unused import
import java.time.zone.ZoneRulesProvider;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/")
public class StaffController {
    //TODO: field injection is not recommended
    @Autowired
    RobService robService;

    @PostMapping("staffs")
    public ResponseEntity<String> addNewRob(@RequestBody @Valid Name name) throws URISyntaxException {
        Staff staff = new Staff(name.getFirstName(), name.getLastName());
        robService.saveAndFlushRob(staff);
        return ResponseEntity.status(HttpStatus.CREATED)
                //TODO: string is OK?
                .location(new URI("http://localhost/api/staffs/"+staff.getId()))
                .build();
    }

    @GetMapping("staffs/{staffId}")
    public ResponseEntity<Staff> getRobInfo(@PathVariable Long staffId) throws RobNotFundException {

        //TODO: ???? excuse me?????
        Staff staff = robService.findById(staffId);
        staff.setZoneId(new ZoneId("Asia/Chongqing").getZoneId());

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(staff);
    }

    @GetMapping("staffs")
    public ResponseEntity<List<Staff>> getAllRobInfo() throws RobNotFundException {
        //TODO: ???????? excuse me??
        Staff staff1 = new Staff("Rob1", "Hall");
        robService.saveAndFlushRob(staff1);
        Staff staff2 = new Staff("Rob2", "Hall");
        robService.saveAndFlushRob(staff2);
        ArrayList<Staff> staff = new ArrayList<>();
        staff.add(robService.findById(staff1.getId()));
        staff.add(robService.findById(staff2.getId()));

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(staff);
    }

    @ExceptionHandler(RobNotFundException.class)
    public ResponseEntity handleNotFoundRob() {
        return ResponseEntity.status(404).build();
    }
}
