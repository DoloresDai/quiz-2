package com.twuc.webApp.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "staff")
public class Staff {
    @Id
    @GeneratedValue
    @JsonProperty("id")
    private Long id;

    @Column(nullable = false,length = 64)
    private String firstName;

    @Column(nullable = false,length = 64)
    private String lastName;

    @Column
    private String zoneId;

    public Staff() {
    }

    public Staff(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Staff(String firstName, String lastName, String zoneId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.zoneId = zoneId;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }
}
